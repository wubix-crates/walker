use futures::future::LocalBoxFuture;
use librust::fs::{read_dir, DirEntry, ReadDir};
use librust::io;
use librust::path::{Path, PathBuf, PathExt};
use librust::stream::{unfold, Stream, StreamExt, Unfold};
use std::{
    cell::RefCell,
    pin::Pin,
    rc::Rc,
    task::{Context, Poll},
};

type InnerStreamFuture = LocalBoxFuture<'static, Option<(io::Result<DirEntry>, ())>>;

pub struct Walker {
    root: PathBuf,
    cur: Rc<RefCell<Option<ReadDir>>>,
    stack: Rc<RefCell<Vec<ReadDir>>>,
    inner: Unfold<(), Box<dyn FnMut(()) -> InnerStreamFuture>, InnerStreamFuture>,
}

fn walk(
    cur: Rc<RefCell<Option<ReadDir>>>,
    stack: Rc<RefCell<Vec<ReadDir>>>,
) -> LocalBoxFuture<'static, Option<io::Result<DirEntry>>> {
    Box::pin(async move {
        match &mut *cur.borrow_mut() {
            None => return None,
            Some(ref mut d) => match d.next().await {
                Some(Err(e)) => return Some(Err(e)),
                Some(Ok(entry)) => {
                    let path = entry.path();
                    if path.clone().as_path().is_dir().await {
                        match read_dir(path).await {
                            Err(e) => return Some(Err(e)),
                            Ok(next) => stack.borrow_mut().push(next),
                        };
                    }

                    return Some(Ok(entry));
                }
                None => {}
            },
        }

        let mut stack_lock = stack.borrow_mut();
        match stack_lock.pop() {
            None => None,
            Some(next) => {
                *cur.borrow_mut() = Some(next);
                drop(stack_lock);
                walk(cur, stack).await
            }
        }
    })
}

impl Walker {
    pub async fn new(path: &Path) -> io::Result<Self> {
        let cur = Rc::new(RefCell::new(Some(read_dir(path).await?)));
        let stack = Rc::new(RefCell::new(Vec::new()));
        Ok(Walker {
            root: path.to_path_buf(),
            cur: Rc::clone(&cur),
            stack: Rc::clone(&stack),
            inner: unfold(
                (),
                Box::new(move |()| {
                    let cur = Rc::clone(&cur);
                    let stack = Rc::clone(&stack);
                    Box::pin(async move { walk(cur, stack).await.map(|x| (x, ())) })
                }),
            ),
        })
    }

    pub async fn rewind(&mut self) -> io::Result<()> {
        *self.cur.borrow_mut() = Some(read_dir(self.root.as_path()).await?);
        *self.stack.borrow_mut() = Vec::new();
        Ok(())
    }
}

impl Stream for Walker {
    type Item = io::Result<DirEntry>;

    fn poll_next(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
    ) -> Poll<Option<io::Result<DirEntry>>> {
        Pin::new(&mut self.inner).poll_next(context)
    }
}

#[cfg(test)]
mod test {
    use super::Walker;
    use std::env::temp_dir;
    use std::fs;

    macro_rules! check {
        ($e:expr) => {
            match $e {
                Ok(t) => t,
                Err(e) => panic!("{} failed with: {}", stringify!($e), e),
            }
        };
    }

    #[test]
    fn test_walker() {
        let mut tmp = temp_dir();
        tmp.push("walk_dir");
        let dir = tmp.as_path();
        check!(fs::create_dir(dir));

        let dir1 = &dir.join("01/02/03");
        check!(fs::create_dir_all(dir1));
        check!(fs::File::create(&dir1.join("04")));

        let dir2 = &dir.join("11/12/13");
        check!(fs::create_dir_all(dir2));
        check!(fs::File::create(&dir2.join("14")));

        let files = check!(Walker::new(dir));
        let mut cur = [0; 2];
        for f in files {
            let f = f.unwrap().path();
            let stem = f.file_stem().unwrap().to_str().unwrap();
            let root = stem.as_bytes()[0] - b'0';
            let name = stem.as_bytes()[1] - b'0';
            assert!(cur[root as usize] < name);
            cur[root as usize] = name;
        }

        check!(fs::remove_dir_all(dir));
    }
}
